

'use client'

import { ChakraProvider } from '@chakra-ui/react'

//Provider de ChakraUI

export const ChakraProvider__ = (

    { children }: 
    { children : React.ReactNode }

) => 
    
    <ChakraProvider>
        {children}
    </ChakraProvider>