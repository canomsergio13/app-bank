import { getBanks } from "@/services/banks";
import { create } from "zustand";

export const useBankStore = create<BankState>((set, get) => ({

    //estados
    banks: [],
    search: "",

    //funciones para setear estados
    getBanks: async () => {
        //traemos los datos de los bancos de la API

        const banksApi = await getBanks()

        //ordenamos los bancos
        const banks = banksApi.sort((a: Bank, b: Bank) =>
            a.bankName.localeCompare(b.bankName)
        );

        //seteamos el estado
        set( state => ({ ...state, banks }));
    },

    //seteamos el estado de la palabra del banco a buscar
    setSearch: (search: string) : void => set( state => ({ ...state, search })),

    //funcion para eliminar un banco
    deleteBank:  (bankData: Bank[], bankName: string) : void => {

        const banks = bankData.filter(bank => bank.bankName !== bankName);

        set( state => ({ ...state, banks }))
    }
    

}));