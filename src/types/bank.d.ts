

interface Bank {
    description: string;
    age: number;
    url: string;
    bankName: string;
    action?: () => void
}

interface BankState {
    banks: Bank[];
    search: string;
    getBanks: () => Promise<void>;
    setSearch:  ( searchTerm: string) => void;
    deleteBank:  ( banks: Bank[], bankName: string) => void;
}