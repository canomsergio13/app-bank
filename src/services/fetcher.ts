import axios, { AxiosInstance, AxiosError, AxiosRequestConfig } from "axios";

const axiosAPIInstance: AxiosInstance = axios.create({
   /*  baseURL: 'https://dev.obtenmas.com/catom/api', */
    baseURL: '/api',
});

const fetcher = async (args: AxiosRequestConfig): Promise<any> => {

    try {

        const response = await axiosAPIInstance(args);
        return response;

    } catch (error) {
        const err: AxiosError = error as AxiosError;
        return err.response || err.request || err.message;
    }

};

export default fetcher;

