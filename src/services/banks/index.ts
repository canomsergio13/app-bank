import fetcher from "../fetcher";
import { http_codes } from "../../utils/http-codes";

export const getBanks = async (): Promise<any> => {
    /* const url: string = `/challenge/banks`; */
    const url: string = `/getDataBank`;
    let response: Bank[] = [];
    
    try {
        const fetch = await fetcher({
            url,
            method: 'GET'
        });

        if (fetch.status === http_codes.STATUS_OK){
            response = fetch.data;
        }

        console.log(response)

        return response;
    } catch (error) {
        return "Error al hacer la peticion";
    }
}
