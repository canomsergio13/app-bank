import { Button, Card, CardBody, Heading, Stack, Text } from "@chakra-ui/react"
import Image from "next/image"
import card from '../styles/card.module.css'

const CardBank = ({ url, bankName, description, age, action } : Bank) => 

    <Card minW={200} minH={350} maxH={350} maxW={300} style={{ margin: 10 }}>
        <CardBody>
            <aside  className={card.container__img__}  >
                <Image
                    src={url}
                    alt={bankName}
                    className={card.img__}
                    width={120}
                    height={120}
                    priority
                />
            </aside>
            <Stack mt='6' spacing='3'>
                <Heading className={card.text_center} size='md'>{bankName }</Heading>
                <Text  className={card.text_center}>
                    {description }
                </Text>
                <Text color='green.600' className={card.text_center} fontSize='1xl'>
                    Años: {age }
                </Text>
                <Button onClick={action} size='xs' colorScheme='red'>Eliminar Banco</Button>
            </Stack>
        </CardBody>
    </Card>


export default CardBank