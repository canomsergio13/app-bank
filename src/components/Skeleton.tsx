
import { generateUniqueId } from '@/helpers/getId'
import skeleton from '../styles/skeleton.module.css'

const Skeleton = () =>

    <div className={skeleton.card__}>
        <div className={`${skeleton.card__skeleton} ${skeleton.card__description}`}>
        </div>
        {
            Array.from({ length: 4 }, _ => 
                <div key={generateUniqueId()} className={`${skeleton.card__skeleton} ${skeleton.card__title}`}>
                </div>
            )
        }
    </div>


export default Skeleton

