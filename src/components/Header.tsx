"use client"

import { Input } from "@chakra-ui/react";
import { useBankStore } from "../store/bankStore";

const headerStyle = {
    backgroundColor: '#05163d',
    color: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px 20px',
    alignItems: 'center'
};

const leftStyle = {
    fontWeight: 'bold'
};

const rightStyle = {
    fontWeight: 'bold'
};

const inputStyle = {
    margin: '0px 20px'
};

const  Header = () => {

    const { setSearch, search } = useBankStore();
  
    const handleSearchChange = (e:  React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
    };

    return (
        <header style={headerStyle}>
            <section style={leftStyle}>Bancos</section>
            <Input 
                style={inputStyle}  
                placeholder='Busca un Banco'
                type="text"
                value={search}
                onChange={handleSearchChange}
            />
            <section style={rightStyle}>Apps</section>
        </header>
    );
}



export default Header;
