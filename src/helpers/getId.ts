
//funcion para genear id
export function generateUniqueId(): string {
    
    const timestamp: number = new Date().getTime();
    const randomNum: number = Math.random() * 1000000; 
    
    const uniqueId: string = timestamp.toString() + randomNum.toString();
    
    return uniqueId;
}