import { NextResponse } from "next/server";

export async function GET(request) {
    try {
        const response = await fetch('https://dev.obtenmas.com/catom/api/challenge/banks');

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();

        return NextResponse.json(data)

    } catch (error) {
        return NextResponse.json({
            error
        })
    }
}
