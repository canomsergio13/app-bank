"use client"

import styles from '../styles/page.module.css'
import { useBankStore } from "../store/bankStore";
import { useEffect, useState } from 'react';
import CardBank from '@/components/CardBank';
import { generateUniqueId } from '@/helpers/getId';
import Skeleton from '@/components/Skeleton';

export default function Home() {

	//funciones para modificar estados
	const { getBanks, deleteBank } = useBankStore();

	//estados
	const [Load, setLoad] = useState(true)
	const { banks, search } = useBankStore();

	useEffect( () =>{ 
		getBanks() 
		setLoad(false)
	} , [])

	const resultBanks = banks.filter(bank => bank.bankName.toLowerCase().includes(search.toLowerCase().trim()));

	const deleteBank__ = (bankName: string) => deleteBank(banks, bankName )
	
	return (
		<main className={styles.main}>
			{
				Load 
					?   Array.from({ length: 5 }, _ => <Skeleton key={generateUniqueId()} />)

					:   resultBanks.map( bank =>
							<CardBank
								key={generateUniqueId()}
								age={bank.age}
								bankName={bank.bankName}
								description={bank.description}
								url={bank.url}
								action={() => deleteBank__(bank.bankName)}
							/>
						)
			}

			{
				!Load && (resultBanks.length === 0 ) && (search !== "")
					? <h1 className={styles.mt_2}>No hay resultados</h1>
					: null

			}
		</main>
	)
}
