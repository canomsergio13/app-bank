import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import '../styles/globals.css'
import { ChakraProvider__ } from '@/providers/ChakraProvider'
import Header from '@/components/Header'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
    title: 'Bancos',
    description: 'Bancos desde una API',
}

export default function RootLayout({
    children,
}: {
    children: React.ReactNode
}) {
    return (
        <html lang="es-MX">
            
            <body className={`${inter.className} general`}>
                <ChakraProvider__>
                    <Header />
                    {children}
                </ChakraProvider__>
            </body>
        </html>
    )
}
